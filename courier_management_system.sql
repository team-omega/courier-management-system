USE [COURIER MANAGEMENT SYSTEM]
-- User Table
CREATE TABLE Users (
    UserID INT PRIMARY KEY,
    Name VARCHAR(255),
    Email VARCHAR(255) UNIQUE,
    Password VARCHAR(255),
    ContactNumber VARCHAR(20),
    Address TEXT
);
-- Inserting data into the User table
INSERT INTO Users (UserID, Name, Email, Password, ContactNumber, Address)
VALUES
(1, 'bhanu', 'bhanu@example.com', 'password123', '1234567890', '123 prasad nagar, vizag'),
(2, 'vinay', 'vinay.smith@example.com', 'vinay123', '9876543210', '456 durga nagar, vizag'),
(3, 'dharani', 'dharani.johnson@example.com', 'securepassword', '5551234567', '789 ganesh nagar, vizag'),
(4, 'pushpa', 'pushpa.brown@example.com', 'password1234', '3339876543', '321 gandhi nagar, vizag'),
(5, 'eswar', 'eswar.davis@example.com', 'eswar123', '1112223333', '654 pradeep nagar, vizag')

-- Courier Table
CREATE TABLE Courier (
    CourierID INT PRIMARY KEY,
    SenderName VARCHAR(255),
    SenderAddress TEXT,
    ReceiverName VARCHAR(255),
    ReceiverAddress TEXT,
    Weight DECIMAL(5, 2),
    Status VARCHAR(50),
    TrackingNumber VARCHAR(20) UNIQUE,
    DeliveryDate DATE
)
ALTER TABLE Courier ADD Packages INT

-- Inserting data into the Courier table
INSERT INTO Courier (CourierID, SenderName, SenderAddress, ReceiverName, ReceiverAddress, Weight, Status, TrackingNumber, DeliveryDate,Packages)
VALUES
(1, 'manu', '123 vizag', 'jassu', '456 delhi', 2.5, 'In Transit', 'TRACK123', '2024-03-05',3),
(2, 'chintu', '456 vizianagaram', 'chaitanya', '789 punjab', 3.0, 'Delivered', 'TRACK456', '2024-03-03',2),
(3, 'bhanu', '789 telangana', 'vinay', '012 bihar', 1.8, 'Pending', 'TRACK789', NULL,1),
(4, 'dharani', '012 hyderabad', 'manohar', '345 srikakulam', 4.2, 'In Transit', 'TRACK012', '2024-03-06',4),
(5, 'jaswanth', '345 poori', 'mary', '678 chennai', 2.0, 'Delivered', 'TRACK345', '2024-03-02',2)
INSERT INTO Courier (CourierID, SenderName, SenderAddress, ReceiverName, ReceiverAddress, Weight, Status, TrackingNumber, DeliveryDate,Packages)
VALUES(6,'chintu','456 vizianagaram','john','624 delhi',3.6,'Delivered','TRACK746','2024-04-19',3)
ALTER TABLE Courier
DROP COLUMN SenderAddress

ALTER TABLE Courier
ALTER COLUMN SenderAddress VARCHAR(100)

INSERT INTO Courier(SenderAddress) VALUES 
('123 vizag'),
('456 vizianagaram'),
('789 telangana'),
('012 hyderabad'),
('345 poori'),
('456 vizianagaram')


-- CourierServices Table
CREATE TABLE CourierServices (
    ServiceID INT PRIMARY KEY,
    ServiceName VARCHAR(100),
    Cost DECIMAL(8, 2)
)
-- Inserting data into the CourierServices table
INSERT INTO CourierServices (ServiceID, ServiceName, Cost)
VALUES
(1, 'Standard', 10.00),
(2, 'Express', 20.00),
(3, 'Overnight', 30.00),
(4, 'International', 50.00),
(5, 'Same Day', 40.00)

-- Employee Table
CREATE TABLE Employee (
    EmployeeID INT PRIMARY KEY,
    Name VARCHAR(255),
    Email VARCHAR(255) UNIQUE,
    ContactNumber VARCHAR(20),
    Role VARCHAR(50),
    Salary DECIMAL(10, 2)
)
-- Inserting data into the Employee table
INSERT INTO Employee (EmployeeID, Name, Email, ContactNumber, Role, Salary)
VALUES
(1, 'JOHN', 'john@example.com', '9998887777', 'Courier Handler', 3000.00),
(2, 'SAM', 'sam@example.com', '8887776666', 'Delivery Driver', 3500.00),
(3, 'JESSI', 'jessi@example.com', '7776665555', 'Manager', 5000.00),
(4, 'VIRAT', 'virat4@example.com', '6665554444', 'Customer Service Representative', 3200.00),
(5, 'VIRAJ', 'viraj@example.com', '5554443333', 'Dispatcher', 4000.00)

-- Location Table
CREATE TABLE Location (
    LocationID INT PRIMARY KEY,
    LocationName VARCHAR(100),
    Address TEXT
)
-- Inserting data into the Location table
INSERT INTO Location (LocationID, LocationName, Address)
VALUES
(1, 'near-honda showroom', '123 prasad nagar, vizag'),
(2, 'near-hero showroom', '456 durga nagar, vizag'),
(3, 'y-junction', '789 ganesh nagar, vizag'),
(4, 'near-ibaco', '321 gandhi nagar, vizag'),
(5, 'near-police station', '654 pradeep nagar, vizag')

-- Payment Table
CREATE TABLE Payments (
    PaymentID INT PRIMARY KEY,
    CourierID INT,
    LocationID INT,
    Amount DECIMAL(10, 2),
    PaymentDate DATE,
    FOREIGN KEY (CourierID) REFERENCES Courier(CourierID),
    FOREIGN KEY (LocationID) REFERENCES Location(LocationID)
)
EXEC sp_rename 'Payment','Payments'
-- Inserting data into the Payment table
INSERT INTO Payments(PaymentID, CourierID, LocationID, Amount, PaymentDate)
VALUES
(1, 1, 2, 25.00, '2024-03-01'),
(2, 2, 3, 30.00, '2024-03-02'),
(3, 3, 4, 15.00, '2024-03-03'),
(4, 4, 5, 20.00, '2024-03-04'),
(5, 5, 1, 35.00, '2024-03-05')

--TASK 2

--1)list all customers
SELECT * FROM Users

--2)List all orders for a specific customer
SELECT * FROM Courier WHERE SenderName = 'bhanu' OR ReceiverName = 'vinay'

--3)lit all couriers
SELECT * FROM Courier

--4)List all packages for a specific order
SELECT * FROM Courier WHERE CourierID = '4'

--5)List all deliveries for a specific courier
SELECT * FROM Payments WHERE CourierID = '3'

--6). List all undelivered packages
SELECT * FROM Courier WHERE Status != 'Delivered'

--8)List all packages with a specific status
SELECT * FROM Courier WHERE Status = 'Delivered'

--9)Calculate the total number of packages for each courier 
SELECT CourierID, COUNT(*) AS TotalPackages FROM Courier GROUP BY CourierID

---10)Find the average delivery time for each courier 
select c.CourierID,AVG(DATEDIFF(day,p.PaymentDate,c.DeliveryDate)) [AvgDelivery Time]
from Courier c
left join Payments p ON c.CourierID = p.CourierID
group by c.CourierID


--11)List all packages with a specific weight range
SELECT * FROM Courier WHERE Weight BETWEEN 2.0 AND 5.0

--12) Retrieve employees whose names contain 'John'
SELECT * FROM Employee WHERE Name LIKE '%John%'

--13) Retrieve all courier records with payments greater than $50
SELECT * FROM Payments WHERE Amount > 50

--TASK 3
--14. Find the total number of couriers handled by each employee.  
SELECT EmployeeID, COUNT(*) AS TotalCouriers FROM Courier GROUP BY EmployeeID

--15. Calculate the total revenue generated by each location  
SELECT LocationID, SUM(Amount) AS TotalRevenue FROM Payments GROUP BY LocationID

--16. Find the total number of couriers delivered to each location. 
SELECT LocationID, COUNT(*) AS TotalCouriersDelivered FROM Payments GROUP BY LocationID

--17. Find the courier with the highest average delivery time
--select TOP 1 c.CourierID,AVG((DATEDIFF(day, c.DeliveryDate,p.PaymentDate))) AS AvgDeliveryTime
--from Courier c
--LEFT JOIN Payments p ON c.CourierID = p.CourierID
--GROUP BY c.CourierID
--ORDER BY AVG((DATEDIFF(day, c.DeliveryDate,p.PaymentDate))) DESC

--18. Find Locations with Total Payments Less Than a Certain Amount  
SELECT LocationID, SUM(Amount) AS TotalPayments 
FROM Payments 
GROUP BY LocationID 
HAVING SUM(Amount) < 200

--19. Calculate Total Payments per Location  
SELECT LocationID, SUM(Amount) AS TotalPayments 
FROM Payments 
GROUP BY LocationID

--20. Retrieve couriers who have received payments totaling more than $1000 in a specific location 
SELECT CourierID 
FROM Payments 
WHERE LocationID = 1
GROUP BY CourierID 
HAVING SUM(Amount) > 100

--21. Retrieve couriers who have received payments totaling more than $1000 after a certain date 
--(PaymentDate > 'YYYY-MM-DD'): 
SELECT *FROM Payments
SELECT CourierID 
FROM Payments 
WHERE PaymentDate > '2024-03-02' 
GROUP BY CourierID
HAVING SUM(Amount) > 500

--22. Retrieve locations where the total amount received is more than $5000 before a certain date 
SELECT LocationID 
FROM Payments 
WHERE PaymentDate > '2024-03-03' 
GROUP BY LocationID 
HAVING SUM(Amount) > 700

--TASK 4---
--23. Retrieve Payments with Courier Information 
SELECT *FROM Payments
SELECT *FROM Courier
SELECT PaymentID,LocationID,Amount,PaymentDate,p.CourierID,
		SenderName,SenderAddress,ReceiverName,ReceiverAddress,Weight,Status,TrackingNumber,DeliveryDate
FROM Payments p
JOIN Courier c ON P.CourierID=c.CourierID


--24. Retrieve Payments with Location Information 
SELECT *FROM Payments
SELECT *FROM Location
SELECT PaymentID,p.LocationID,Amount,PaymentDate,CourierID,
		LocationName,Address
FROM Payments p
JOIN Location l ON P.CourierID=l.LocationID

--25. Retrieve Payments with Courier and Location Information
SELECT *FROM Payments
SELECT *FROM Location
SELECT PaymentID,p.LocationID,Amount,PaymentDate,p.CourierID,
		LocationName,Address,SenderName,SenderAddress,ReceiverName,
		ReceiverAddress,Weight,Status,TrackingNumber,DeliveryDate
FROM Payments p
JOIN Location l ON P.CourierID=l.LocationID
JOIN Courier c ON P.CourierID=c.CourierID


--26.  List all payments with courier details
SELECT *FROM Payments
SELECT *FROM Courier
SELECT Amount,p.CourierID,SenderName,ReceiverName,Weight,Status,Packages
FROM Payments p
JOIN Courier c ON p.CourierID=c.CourierID


--27. Total payments received for each courier  
SELECT *FROM Payments
SELECT *FROM Courier
SELECT c.CourierID,SUM(Amount) [TOATL PAYMENT]
FROM Courier c
JOIN Payments p ON p.CourierID=c.CourierID
GROUP BY c.CourierID


--28.  List payments made on a specific date 
SELECT *FROM Payments
WHERE PaymentDate='2023-03-02'


--29. Get Courier Information for Each Payment 
SELECT p.PaymentID, p.CourierID, p.LocationID, p.Amount, p.PaymentDate,
       c.SenderName, c.SenderAddress, c.ReceiverName, c.ReceiverAddress,
       c.Weight, c.Status, c.TrackingNumber, c.DeliveryDate
FROM Payments p
JOIN Courier c ON p.CourierID = c.CourierID


--30.  Get Payment Details with Location  
SELECT *FROM Payments
SELECT Amount,PaymentID,p.LocationID,LocationName,l.Address
FROM Payments p
JOIN Location l ON p.LocationID=l.LocationID


--31.  Calculating Total Payments for Each Courier 
SELECT *FROM Payments
SELECT *FROM Courier
SELECT c.CourierID,SUM(Amount) [TOATL PAYMENT]
FROM Courier c
JOIN Payments p ON p.CourierID=c.CourierID
GROUP BY c.CourierID


---32. List Payments Within a Date Range 
SELECT *FROM Payments
SELECT PaymentID,Amount 
FROM Payments WHERE PaymentDate>='2023-03-03' AND PaymentDate<='2024-03-04'


---33. Retrieve a list of all users and their corresponding courier records, including cases where there are 
--no matches on either side 
SELECT u.UserID, u.Name AS UserName, c.CourierID, c.SenderName, c.ReceiverName
FROM Users u
FULL OUTER JOIN Courier c ON u.UserID = c.CourierID


--34. Retrieve a list of all couriers and their corresponding services, including cases where there are no 
--matches on either side  
SELECT *FROM CourierServices
SELECT c.CourierID, c.SenderName, c.ReceiverName, s.ServiceID, s.ServiceName, s.Cost
FROM Courier c
FULL OUTER JOIN CourierServices s ON c.CourierID = s.ServiceID


---35.  Retrieve a list of all employees and their corresponding payments, including cases where there are 
---no matches on either side
SELECT e.EmployeeID, e.Name, p.PaymentID, p.Amount, p.PaymentDate
FROM Employee e
FULL OUTER JOIN Payments p ON e.EmployeeID = p.PaymentID

---36. List all users and all courier services, showing all possible combinations
SELECT u.UserID, u.Name, cs.ServiceID, cs.ServiceName, cs.Cost
FROM Users u
CROSS JOIN CourierServices cs


---37. List all employees and all locations, showing all possible combinations
SELECT e.EmployeeID, e.Name AS EmployeeName, l.LocationID, l.LocationName, l.Address
FROM Employee e
CROSS JOIN Location l

---38. Retrieve a list of couriers and their corresponding sender information   
SELECT *FROM Courier
SELECT c.CourierID, c.SenderName, c.SenderAddress
FROM Courier c
JOIN Users u ON c.CourierID = u.UserID


---39. Retrieve a list of couriers and their corresponding Receiver information   
SELECT *FROM Courier
SELECT c.CourierID, c.ReceiverName, c.ReceiverAddress
FROM Courier c
JOIN Users u ON c.CourierID = u.UserID


---40. Retrieve a list of couriers along with the courier service details
SELECT *FROM Courier
SELECT *FROM CourierServices
SELECT c.CourierID, c.SenderName, c.ReceiverName, cs.ServiceID, cs.ServiceName, cs.Cost
FROM Courier c
JOIN CourierServices cs ON c.CourierID = cs.ServiceID


---41. Retrieve a list of employees and the number of couriers assigned to each employee:
SELECT *FROM Employee
SELECT *FROM Courier
SELECT e.EmployeeID, e.Name, COUNT(c.CourierID) [numberm of couriers]
FROM Employee e
LEFT JOIN Courier c ON e.EmployeeID = c.EmployeeID
GROUP BY e.EmployeeID, e.Name


---42.  Retrieve a list of locations and the total payment amount received at each location
SELECT l.LocationID, l.LocationName, SUM(p.Amount) AS TotalPaymentAmount
FROM Location l
LEFT JOIN Payments p ON l.LocationID = p.LocationID
GROUP BY l.LocationID, l.LocationName

---43. Retrieve all couriers sent by the same sender
SELECT c1.CourierID,c1.SenderName,c1.Packages
FROM Courier c1
JOIN Courier c2 ON c1.SenderName = c2.SenderName
WHERE c1.CourierID <> c2.CourierID

---44. List all employees who share the same role.  
SELECT e1.EmployeeID, e1.Name, e1.Role
FROM Employee e1
JOIN Employee e2 ON e1.Role = e2.Role
WHERE e1.EmployeeID <> e2.EmployeeID


---45. Retrieve all payments made for couriers sent from the same location
SELECT p.PaymentID, p.CourierID, p.LocationID, p.Amount, p.PaymentDate
FROM Payments p
JOIN Courier c1 ON p.CourierID = c1.CourierID
JOIN Courier c2 ON c1.SenderAddress = c2.SenderAddress
WHERE c1.SenderAddress <> c2.SenderAddress


---46. Retrieve all couriers sent from the same location
SELECT c1.CourierID,c1.SenderName,c1.SenderAddress,c1.Packages
FROM Courier c1
JOIN Courier c2 ON c1.SenderAddress = c2.SenderAddress
WHERE c1.CourierID <> c2.CourierID


---47.  List employees and the number of couriers they have delivered List employees and the number of couriers they have delivered
SELECT e.EmployeeID, e.Name, COUNT(c.CourierID)  [Num Of Couriers Delivered]
FROM Employee e
LEFT JOIN Courier c ON e.EmployeeID = c.EmployeeID
where c.Status = 'Delivered'
GROUP BY e.EmployeeID, e.Name


---48. Find couriers that were paid an amount greater than the cost of their respective courier services 
SELECT c.CourierID, c.SenderName, c.ReceiverName, p.Amount AS PaymentAmount, cs.Cost AS CourierServiceCost
FROM Courier c
JOIN Payments p ON c.CourierID = p.CourierID
JOIN CourierServices cs ON c.CourierID = cs.ServiceID
WHERE p.Amount > cs.Cost


--49. Find couriers that have a weight greater than the average weight of all couriers
SELECT *
FROM Courier
WHERE Weight > (SELECT AVG(Weight) FROM Courier)

--50. Find the names of all employees who have a salary greater than the average salary:
SELECT Name
FROM Employee
WHERE Salary > (SELECT AVG(Salary) FROM Employee)

--51. Find the total cost of all courier services where the cost is less than the maximum cost
SELECT SUM(Cost)
FROM CourierServices 
WHERE Cost < (SELECT MAX(Cost) FROM CourierServices)

--52. Find all couriers that have been paid for
SELECT *
FROM Courier
WHERE CourierId IN (SELECT DISTINCT CourierId FROM Payments)

--53. Find the locations where the maximum payment amount was made
SELECT LocationId
FROM Payments
WHERE Amount = (SELECT MAX(Amount) FROM Payments)

--54. Find all couriers whose weight is greater than the weight of all couriers sent by a specific sender 
SELECT *
FROM Courier c
WHERE Weight > ALL (SELECT Weight FROM Courier  WHERE SenderName = 'manu')







